<?php
    require_once("config.php");


		$query = $db->prepare("
			SELECT category_id, name	
			FROM categories
		");

		$query->execute();

		$categories = $query->fetchAll( PDO::FETCH_ASSOC);

		/* echo '<pre>';	print_r($categories);print "</pre>"; */

    if ( isset($_POST["submit"]) ) {
        if (
            strlen($_POST["name"]) <= 255 &&
            is_numeric($_POST["price"]) > 0 &&
			isset($_FILES["image"]) &&
			is_numeric($_POST["category_id"]) > 0 
        ) {
			$allowed_files = array (
                "jpg" => "image/jpeg", 
                "png" => "image/png"
            );

			if (  $_FILES["image"]["error"] === 0 &&
                    $_FILES["image"]["size"] > 0 &&
                    $_FILES["image"]["size"] <= 1024000 &&
                    in_array($_FILES["image"]["type"], $allowed_files) 
            ) {

                $extension = explode("/", $_FILES["image"]["type"]);
                $filename = date("YmdHis") . "_" . mt_rand(10000,99999) . "." . $extension[1];

                move_uploaded_file( $_FILES["image"]["tmp_name"], "images/". $filename ); 
            }
           
		   /* echo '<pre>';	print_r($_FILES);print "</pre>"; */
                
				$query = $db -> prepare("
                    INSERT INTO products
                    (name, price, image, category_id )
                    VALUES( ?, ?, ?, ?)
                ");

                $insert = $query->execute(
                    array (
                        strtolower( trim($_POST["name"]) ),
                        (float)$_POST["price"] ,
                        strtolower( trim($filename) ),
						(int)$_POST["category_id"]

                    )
                );

                $message = ("produto adicionado");
      
        }
        else {
            $message = ("faltam campos");
        }
    } 
?>
<!DOCTYPE HTML>
<html>
<head>
<title>Login</title>
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<link href="css/style.css" rel='stylesheet' type='text/css' />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<script src="js/jquery.min.js"></script>
<script type="text/javascript">
        $(document).ready(function() {
            $(".dropdown img.flag").addClass("flagvisibility");

            $(".dropdown dt a").click(function() {
                $(".dropdown dd ul").toggle();
            });
                        
            $(".dropdown dd ul li a").click(function() {
                var text = $(this).html();
                $(".dropdown dt a span").html(text);
                $(".dropdown dd ul").hide();
                $("#result").html("Selected value is: " + getSelectedValue("sample"));
            });
                        
            function getSelectedValue(id) {
                return $("#" + id).find("dt a span.value").html();
            }

            $(document).bind('click', function(e) {
                var $clicked = $(e.target);
                if (! $clicked.parents().hasClass("dropdown"))
                    $(".dropdown dd ul").hide();
            });


            $("#flagSwitcher").click(function() {
                $(".dropdown img.flag").toggleClass("flagvisibility");
            });
        });
     </script>
 </head>
<body>
	<div class="header">
		<div class="container">
			<div class="row">
			  <div class="col-md-12">
				 <div class="header-left">
					 <div class="logo">
						<a href="admin.php"><img src="images/logo.png" alt=""/></a>
					 </div>
					 <div class="menu">
						  <a class="toggleMenu" href="#"><img src="images/nav.png" alt="" /></a>

							<script type="text/javascript" src="js/responsive-nav.js"></script>
				    </div>							
	    		    <div class="clear"></div>
	    	    </div>
	            <div class="header_right">
	    		  <!-- start search-->
				 
						</div>
						<!----search-scripts---->
						<script src="js/classie.js"></script>
						<script src="js/uisearch.js"></script>
						<script>
							new UISearch( document.getElementById( 'sb-search' ) );
						</script>
				    <ul class="icon1 sub-icon1 profile_img">
					 <li><a  href="#"> </a>
						<ul class="sub-icon1 list">
						  <div class="product_control_buttons">
						  	<a href="#"><img src="images/edit.png" alt=""/></a>
						  		<a href="#"><img src="images/close_edit.png" alt=""/></a>
						  </div>
						   <div class="clear"></div>
						  <li class="list_img"><img src="images/1.jpg" alt=""/></li>
						  <li class="list_desc"><h4><a href="#">velit esse molestie</a></h4><span class="actual">1 x
                          $12.00</span></li>
						  <div class="login_buttons">
							 <div class="check_button"><a href="checkout.html">Check out</a></div>
							 <div class="login_button"><a href="login.html">Login</a></div>
							 <div class="clear"></div>
						  </div>
						  <div class="clear"></div>
						</ul>
					 </li>
				   </ul>
		        <div class="clear"></div>
	       </div>
	      </div>
		 </div>
	    </div>
	  </div>
     <div class="main">
      <div class="shop_top">
		<div class="container">
			<div class="col-md-6">
				 <div class="login-page">
			    	 <div class="row">
						<div class="col-md-12 contact">
				  			<form method="post" action="<?php echo $_SERVER["REQUEST_URI"]; ?>" enctype="multipart/form-data" >
								<div class="to">

<?php if(isset($message)) {
    echo '<p>' .$message. '</p>';
   
}
?>

						<label>Name
                     	<input type="text" class="text" name="name" required>
						 </label>
						 <label>Price
					 	<input type="text" class="text" name="price" required>
					 	</label>
						 <label>Image
						 <input type="file" class="text" name="image" required>
						 </label>
						 <label>ID do produte
						 <select type="text" class="text" name="category_id" required>
<?php 
    foreach($categories as $category) {
?>
						 	<option><?php echo $category["category_id"];?></option>
							
<?php 
	}
?>
						 </select> 
						 </label>
					   <div class="form-submit">
			           <input name="submit" type="submit" id="submit" ><br>
			           </div>
	                </div>
	                <div class="clear"></div>
                   </form>
				 <table border="2">
	                <tr>
                     <th>ID do produte</th>
		           <th>artigo</th>
<?php 
    foreach($categories as $category) {
?>
				 	<tr>
                        <td><?php echo $category["category_id"];?></td>  
				        <td><?php echo $category["name"];?></td>
					</tr>
<?php 
	}
?>
				</table>
			   </div>
   		     </div>
		    </div>
	     </div>
	   </div>
	  </div>
	  <div class="footer">
			<div class="container">
				<div class="row">
					<div class="col-md-3">
						<ul class="footer_box">
							<h4>Products</h4>
							<li><a href="#">Mens</a></li>
							<li><a href="#">Womens</a></li>
							<li><a href="#">Youth</a></li>
						</ul>
					</div>
					<div class="col-md-3">
						<ul class="footer_box">
							<h4>About</h4>
							<li><a href="#">Careers and internships</a></li>
							<li><a href="#">Sponserships</a></li>
							<li><a href="#">team</a></li>
							<li><a href="#">Catalog Request/Download</a></li>
						</ul>
					</div>
					<div class="col-md-3">
						<ul class="footer_box">
							<h4>Customer Support</h4>
							<li><a href="#">Contact Us</a></li>
							<li><a href="#">Shipping and Order Tracking</a></li>
							<li><a href="#">Easy Returns</a></li>
							<li><a href="#">Warranty</a></li>
							<li><a href="#">Replacement Binding Parts</a></li>
						</ul>
					</div>
					<div class="col-md-3">
						<ul class="footer_box">
							<h4>Newsletter</h4>
							<div class="footer_search">

					        </div>
							<ul class="social">	
							  <li class="facebook"><a href="#"><span> </span></a></li>
							  <li class="twitter"><a href="#"><span> </span></a></li>
							  <li class="instagram"><a href="#"><span> </span></a></li>	
							  <li class="pinterest"><a href="#"><span> </span></a></li>	
							  <li class="youtube"><a href="#"><span> </span></a></li>										  				
						    </ul>
		   				</ul>
					</div>
				</div>
				<div class="row footer_bottom">
				    <div class="copy">
			           <p>© 2014 Template by <a href="http://w3layouts.com" target="_blank">w3layouts</a></p>
		            </div>
					  <dl id="sample" class="dropdown">
				        <dt><a href="#"><span>Change Region</span></a></dt>
				        <dd>
				            <ul>
				                <li><a href="#">Australia<img class="flag" src="images/as.png" alt="" /><span class="value">AS</span></a></li>
				                <li><a href="#">Sri Lanka<img class="flag" src="images/srl.png" alt="" /><span class="value">SL</span></a></li>
				                <li><a href="#">Newziland<img class="flag" src="images/nz.png" alt="" /><span class="value">NZ</span></a></li>
				                <li><a href="#">Pakistan<img class="flag" src="images/pk.png" alt="" /><span class="value">Pk</span></a></li>
				                <li><a href="#">United Kingdom<img class="flag" src="images/uk.png" alt="" /><span class="value">UK</span></a></li>
				                <li><a href="#">United States<img class="flag" src="images/us.png" alt="" /><span class="value">US</span></a></li>
				            </ul>
				         </dd>
	   				  </dl>
   				</div>
			</div>
		</div>
</body>	
</html>


