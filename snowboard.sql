-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 18-Abr-2017 às 17:40
-- Versão do servidor: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `snowboard`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(10) UNSIGNED NOT NULL,
  `username` varchar(64) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `admin`
--

INSERT INTO `admin` (`admin_id`, `username`, `password`) VALUES
(4, 'admin', '$2y$10$nbkSQcibksV37L8MeRzD.e3lQKaVdHDhkM4DL4zIYpu70RiN.Fviy');

-- --------------------------------------------------------

--
-- Estrutura da tabela `categories`
--

CREATE TABLE `categories` (
  `category_id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `categories`
--

INSERT INTO `categories` (`category_id`, `name`) VALUES
(1, 'Snowboards'),
(2, 'Jaquetas ');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contacts`
--

CREATE TABLE `contacts` (
  `email` varchar(252) NOT NULL,
  `subject` varchar(128) NOT NULL,
  `message` text NOT NULL,
  `contact_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `contacts`
--

INSERT INTO `contacts` (`email`, `subject`, `message`, `contact_id`, `name`) VALUES
('asd@asd.com', 'coise', 'met, Loremmet, Loremmet, Lorem', 1, 'snow'),
('asd@as2d.com', 'coise1', '00 Lorem Ipsum Dolor Sit,\r\n22-56-2-9 Sit Amet, Lorem,00 Lorem Ipsum Dolor Sit,\r\n22-56-2-9 Sit Amet, Lorem,00 Lorem Ipsum Dolor Sit,\r\n22-56-2-9 Sit Amet, Lorem,00 Lorem Ipsum Dolor Sit,\r\n22-56-2-9 Sit Amet, Lorem,00 Lorem Ipsum Dolor Sit,\r\n22-56-2-9 Sit Amet, Lorem,00 Lorem Ipsum Dolor Sit,\r\n22-56-2-9 Sit Amet, Lorem,00 Lorem Ipsum Dolor Sit,\r\n22-56-2-9 Sit Amet, Lorem,', 2, 'snow2'),
('asasa@dasd.com', 'boardsws', 'si enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. du', 3, 'john'),
('coise3a@hotmail.com', 'testa', 'barabtanas malookas', 4, 'coise');

-- --------------------------------------------------------

--
-- Estrutura da tabela `orderdetails`
--

CREATE TABLE `orderdetails` (
  `order_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `quantity` int(6) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `size` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `orderdetails`
--

INSERT INTO `orderdetails` (`order_id`, `product_id`, `quantity`, `price`, `size`) VALUES
(1, 5, 1, '999.99', 'Small'),
(2, 5, 1, '999.99', 'Small'),
(3, 5, 1, '999.99', 'Small'),
(4, 5, 1, '999.99', 'Small'),
(5, 1, 3, '100.99', 'Standard '),
(6, 1, 1, '100.99', 'Standard '),
(7, 1, 1, '100.99', 'Standard '),
(8, 1, 1, '100.99', 'Standard '),
(8, 5, 2, '999.99', 'Small'),
(9, 1, 1, '100.99', 'Standard ');

-- --------------------------------------------------------

--
-- Estrutura da tabela `orders`
--

CREATE TABLE `orders` (
  `order_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `order_date` datetime NOT NULL,
  `paid` tinyint(1) UNSIGNED NOT NULL,
  `payment` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `orders`
--

INSERT INTO `orders` (`order_id`, `user_id`, `order_date`, `paid`, `payment`) VALUES
(6, 2, '2017-04-02 23:14:55', 0, '0000-00-00 00:00:00'),
(7, 2, '2017-04-02 23:15:04', 0, '0000-00-00 00:00:00'),
(8, 3, '2017-04-18 15:22:47', 0, '0000-00-00 00:00:00'),
(9, 4, '2017-04-18 16:32:29', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `productattributes`
--

CREATE TABLE `productattributes` (
  `productattribute_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `description` varchar(256) NOT NULL,
  `stock` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `productattributes`
--

INSERT INTO `productattributes` (`productattribute_id`, `product_id`, `description`, `stock`) VALUES
(2, 1, 'Standard ', 89),
(3, 2, 'Standard ', 99),
(7, 3, 'Standard ', 79),
(8, 4, 'Sandard ', 69),
(9, 5, 'Small', 40),
(10, 6, 'Large', 50),
(11, 7, 'Medium', 20),
(12, 8, 'Medium', 10),
(15, 7, 'extra-large', 44),
(16, 8, 'extra-large', 44);

-- --------------------------------------------------------

--
-- Estrutura da tabela `products`
--

CREATE TABLE `products` (
  `product_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `image` varchar(255) NOT NULL,
  `category_id` smallint(5) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `products`
--

INSERT INTO `products` (`product_id`, `name`, `description`, `price`, `image`, `category_id`) VALUES
(1, 'Mega-Board 1', 'Alta prancha espectacular !', '100.99', 'board1.jpg', 1),
(2, 'Mega-Board 2', 'Alta prancha espectacular !', '100.99', 'board2.jpg', 1),
(3, 'Mega-Board 3', 'Alta prancha espectacular !', '800.99', 'board3.jpg', 1),
(4, 'Mega-Board 4', 'Alta prancha espectacular !', '60.99', 'board4.jpg', 1),
(5, 'Mega Jaqueta 1', 'Mega jaqueta Ultra espectacular!', '999.99', 'jaqueta1.jpg', 2),
(6, 'Mega Jaqueta 2', 'Mega jaqueta Ultra espectacular!', '499.99', 'jaqueta2.jpg', 2),
(7, 'Mega Jaqueta 3', 'Mega jaqueta Ultra espectacular!', '439.99', 'jaqueta3.jpg', 2),
(8, 'Mega Jaqueta 4', 'Mega jaqueta Ultra espectacular!', '139.99', 'jaqueta4.jpg', 2),
(27, 'dfssdfff', '', '200.97', '20170412104130_34435.jpeg', 1),
(28, 'coisemaster', '', '342.00', '20170418173414_64232.jpeg', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `username` varchar(64) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(128) NOT NULL,
  `email` varchar(252) NOT NULL,
  `phone` varchar(32) NOT NULL,
  `address` varchar(128) NOT NULL,
  `postal_code` varchar(64) NOT NULL,
  `city` varchar(32) NOT NULL,
  `country` varchar(32) NOT NULL,
  `newsletter` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`user_id`, `username`, `password`, `name`, `email`, `phone`, `address`, `postal_code`, `city`, `country`, `newsletter`) VALUES
(2, 'boardMaster', '$2y$10$wChO6wEqmUhNbQDwzgAO0.komgtodTd3XbJq6drbcKdWiV6/3AByK', 'board master', 'boarmaster@hotmail.com', '910203065', 'casa do snowboard', '6541-123', 'jerusalem', '5', ''),
(3, 'boardMaster99', '$2y$10$Als4mQ5Jt/8.IevSkVgD.eEtK/Rn2N4K2xUnPUGiFwNdP3Zf54E2e', 'zé boards', 'boardMaster99@gmail.com', '3246626454353453453453', 'zé bords home', '2221', 'stapat', '6', 'yes'),
(4, 'boardmaster343', '$2y$10$ntg.30oqf2QhVjzmk37qfuD2w/UDdWMa92CoJaKbV7ZEDt.G429QG', 'coiseMAster', 'coise3a@hotmail.com', '32432234234', 'Rua coise master', '24324-234324', 'rio de janeiro', 'brazil', 'yes');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`contact_id`);

--
-- Indexes for table `orderdetails`
--
ALTER TABLE `orderdetails`
  ADD PRIMARY KEY (`order_id`,`product_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `user_id_2` (`user_id`);

--
-- Indexes for table `productattributes`
--
ALTER TABLE `productattributes`
  ADD PRIMARY KEY (`productattribute_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`),
  ADD KEY `categories_id` (`category_id`),
  ADD KEY `categories_id_2` (`category_id`),
  ADD KEY `categories_id_3` (`category_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `contact_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `productattributes`
--
ALTER TABLE `productattributes`
  MODIFY `productattribute_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
