<?php
    require_once("config.php");

    if ( isset($_POST["submit"]) ) {
        foreach($_POST as $key => $value) {

            if(empty($value)) {
                $error = true;
            }
        }

        if (
            !isset($error)  &&
            filter_var($_POST["email"], FILTER_VALIDATE_EMAIL) &&
            strlen($_POST["username"]) <= 64
        ) {
           
            /* die("sdklha");*/
            /* validar o utilizador*/
            $query = $db ->prepare("
                SELECT user_id
                FROM users
                WHERE LOWER(username) = ? OR LOWER(email) = ?
            ");

            $query ->execute(
                array(  
                        strtolower( trim($_POST["username"]) ), 
                        strtolower( trim($_POST["email"]) )
                    )  
            );
            $result = $query->fetchAll(PDO::FETCH_ASSOC);

            if(empty($result)) {

                $query = $db -> prepare("
                    INSERT INTO users
                    (username, password, name, email, phone, address, postal_code, city, country, newsletter)
                    VALUES( ?, ?, ?, ?, ?, ?, ?,  ?, ?, ?)
                ");

                $password = password_hash($_POST["password"], PASSWORD_DEFAULT);

                $insert = $query->execute(
                    array (
                        $_POST["username"],
                        $password,
                        $_POST["name"],
                        $_POST["email"],
                        $_POST["phone"],
                        $_POST["address"],
                        $_POST["postal_code"],
                        $_POST["city"],
                        $_POST["country"],
                        $_POST["newsletter"]
                    )
                );
                header("Location: login.php");
                exit;
            }
            else {
                $message = ("esta conta já foi registada");
            }
        }
        else {
            $message = ("preencha todos os campos");
        }
    }
?>
<!DOCTYPE HTML>
<html>
<head>
<title>Register</title>
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<link href="css/style.css" rel='stylesheet' type='text/css' />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<script src="js/jquery.min.js"></script>
<script type="text/javascript">
        $(document).ready(function() {
            $(".dropdown img.flag").addClass("flagvisibility");

            $(".dropdown dt a").click(function() {
                $(".dropdown dd ul").toggle();
            });
                        
            $(".dropdown dd ul li a").click(function() {
                var text = $(this).html();
                $(".dropdown dt a span").html(text);
                $(".dropdown dd ul").hide();
                $("#result").html("Selected value is: " + getSelectedValue("sample"));
            });
                        
            function getSelectedValue(id) {
                return $("#" + id).find("dt a span.value").html();
            }

            $(document).bind('click', function(e) {
                var $clicked = $(e.target);
                if (! $clicked.parents().hasClass("dropdown"))
                    $(".dropdown dd ul").hide();
            });


            $("#flagSwitcher").click(function() {
                $(".dropdown img.flag").toggleClass("flagvisibility");
            });
        });
     </script>
 </head>
<body>
	<div class="header">
		<div class="container">
			<div class="row">
			  <div class="col-md-12">
				 <div class="header-left">
					 <div class="logo">
						<a href="index.php"><img src="images/logo.png" alt=""/></a>
					 </div>
					 <div class="menu">
						  <a class="toggleMenu" href="#"><img src="images/nav.png" alt="" /></a>
						    <ul class="nav" id="nav">
						    	<li><a href="shop.php">Shop</a></li>
						    	<li><a href="team.php">Team</a></li>
						    	<li><a href="shop.php">Events</a></li>
						    	<li><a href="experiance.php">Experiance</a></li>
						    	<li><a href="categories.php">Company</a></li>
								<li><a href="contact.php">Contact</a></li>								
								<div class="clear"></div>
							</ul>
							<script type="text/javascript" src="js/responsive-nav.js"></script>
				    </div>							
	    		    <div class="clear"></div>
	    	    </div>
	            <div class="header_right">
	    		  <!-- start search-->
				   <div class="search-box">
							<div id="sb-search" class="sb-search">
								<form>
									<input class="sb-search-input" placeholder="Enter your search term..." type="search" name="search" id="search">
									<input class="sb-search-submit" type="submit" value="">
									<span class="sb-icon-search"> </span>
								</form>
							</div>
						</div>
						<!----search-scripts---->
						<script src="js/classie.js"></script>
						<script src="js/uisearch.js"></script>
						<script>
							new UISearch( document.getElementById( 'sb-search' ) );
						</script>
				     <ul class="icon1 sub-icon1 profile_img">
					 <li><a class="active-icon c1" href="#"> </a>
						<ul class="sub-icon1 list">
						  <div class="product_control_buttons">
						  	<a href="#"><img src="images/edit.png" alt=""/></a>
						  		<a href="#"><img src="images/close_edit.png" alt=""/></a>
						  </div>
						   <div class="clear"></div>
						  <li class="list_img"><img src="images/1.jpg" alt=""/></li>
						  <li class="list_desc"><h4><a href="#">velit esse molestie</a></h4><span class="actual">1 x
                          $12.00</span></li>
						  <div class="login_buttons">
							 <div class="check_button"><a href="checkout.php">Check out</a></div>
							 <div class="login_button"><a href="login.php">Login</a></div>
							 <div class="clear"></div>
						  </div>
						  <div class="clear"></div>
						</ul>
					 </li>
				   </ul>
		        <div class="clear"></div>
	       </div>
	      </div>
		 </div>
	    </div>
	  </div>
     <div class="main">
      <div class="shop_top">
	     <div class="container">
<?php if(isset($message)) {
    echo '<p>' .$message. '</p>';
   
}
?>	
			 <p>Already have an account!? <a href="login.php">Login</a></p>
				<form method="post" action="<?php echo $_SERVER["REQUEST_URI"]; ?>">
						<div class="register-top-grid">
								<h3>PERSONAL INFORMATION</h3>
								<div>
									<label>
										Username
										<input type="text" name="username" required>
									</label>
								</div>
								<div>
									<label>
										Password
										<input type="password" name="password" required>
									</label>
								</div>
								<div>
									<label>
										Full Name
										<input type="text" name="name" required>
										</label>
								</div>
								<div>
								 	<label>            
                						Email Address
            							<input type="text" name="email" required>
                					</label>
								</div>
								<div>
									<label>
										Telefone
										<input type="text" name="phone" required>
									</label>
								</div>			
								<div>
									<label>
										Endereço
										<input type="text" name="address" required>
									</label>
								</div>
								<div>
									<label>
										Código Postal
										<input type="text" name="postal_code" required>
									</label>
								</div>
								<div>
									<label>
										Cidade
										<input type="text" name="city" required>
									</label>
								</div>
								<div>
									<label>
										País
										<input type="text" name="country" required>
									</label>
								</div>
								<div>
									<div class="clear"> </div>
											<div class="news-letter" href="#">
												<label class="checkbox">
													Sign Up for Newsletter
													<select name="newsletter">														
														<option>yes</option>
														<option>no</option>
													</select>
												</label>	
											</div>
										<div class="clear"> 
									</div>
								</div>
								<div>
								<input type="submit" name="submit" value="Criar Conta">
								</div>
						</div>
					</form>
				</div>
		   </div>
	  </div>
	  <div class="footer">
			<div class="container">
				<div class="row">
					<div class="col-md-3">
						<ul class="footer_box">
							<h4>Products</h4>
							<li><a href="#">Mens</a></li>
							<li><a href="#">Womens</a></li>
							<li><a href="#">Youth</a></li>
						</ul>
					</div>
					<div class="col-md-3">
						<ul class="footer_box">
							<h4>About</h4>
							<li><a href="#">Careers and internships</a></li>
							<li><a href="#">Sponserships</a></li>
							<li><a href="#">team</a></li>
							<li><a href="#">Catalog Request/Download</a></li>
						</ul>
					</div>
					<div class="col-md-3">
						<ul class="footer_box">
							<h4>Customer Support</h4>
							<li><a href="#">Contact Us</a></li>
							<li><a href="#">Shipping and Order Tracking</a></li>
							<li><a href="#">Easy Returns</a></li>
							<li><a href="#">Warranty</a></li>
							<li><a href="#">Replacement Binding Parts</a></li>
						</ul>
					</div>
					<div class="col-md-3">
						<ul class="footer_box">
							
							<div class="footer_search">
				    		  
					        </div>
							<ul class="social">	
							  <li class="facebook"><a href="#"><span> </span></a></li>
							  <li class="twitter"><a href="#"><span> </span></a></li>
							  <li class="instagram"><a href="#"><span> </span></a></li>	
							  <li class="pinterest"><a href="#"><span> </span></a></li>	
							  <li class="youtube"><a href="#"><span> </span></a></li>										  				
						    </ul>
		   				</ul>
					</div>
				</div>
				<div class="row footer_bottom">
				    <div class="copy">
			           <p>© 2014 Template by <a href="http://w3layouts.com" target="_blank">w3layouts</a></p>
		            </div>
					  <dl id="sample" class="dropdown">
				        <dt><a href="#"><span>Change Region</span></a></dt>
				        <dd>
				            <ul>
				                <li><a href="#">Australia<img class="flag" src="images/as.png" alt="" /><span class="value">AS</span></a></li>
				                <li><a href="#">Sri Lanka<img class="flag" src="images/srl.png" alt="" /><span class="value">SL</span></a></li>
				                <li><a href="#">Newziland<img class="flag" src="images/nz.png" alt="" /><span class="value">NZ</span></a></li>
				                <li><a href="#">Pakistan<img class="flag" src="images/pk.png" alt="" /><span class="value">Pk</span></a></li>
				                <li><a href="#">United Kingdom<img class="flag" src="images/uk.png" alt="" /><span class="value">UK</span></a></li>
				                <li><a href="#">United States<img class="flag" src="images/us.png" alt="" /><span class="value">US</span></a></li>
				            </ul>
				         </dd>
	   				  </dl>
   				</div>
			</div>
		</div>
</body>	
</html>