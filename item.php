<?php
    require_once("config.php");

    if (!isset($_GET["product_id"])) {
        header("Location: ./");
        exit;
    }

    $query = $db-> prepare("
    SELECT p.product_id, p.description, p.name, p.image, p.price
    FROM products AS p
	WHERE product_id = ?
    ");

    $query-> execute( array($_GET["product_id"]) );

    $item  = $query->fetchAll( PDO::FETCH_ASSOC);

	$query = $db-> prepare("
    SELECT stock, description, product_id, productattribute_id
    FROM productattributes 
    WHERE product_id = ?
	");

	$query-> execute ( array($_GET["product_id"]) );

	$attributes = $query->fetchAll( PDO::FETCH_ASSOC);

	if( empty ($attributes) ){
        header("HTTP/1.1 404 Not Found");
        echo'<p>artigo inexistente!!!!<br><a href="./"> Homepage</p>';
        exit;
    }

	//  echo "<pre>"; print_r($attributes); echo "</pre>";
?>
<!DOCTYPE HTML>
<html>
<head>
<title><?php echo $item[0]["name"]?></title>
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<link href="css/style.css" rel='stylesheet' type='text/css' />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<script src="js/jquery.min.js"></script>

<script type="text/javascript">
        $(document).ready(function() {
            $(".dropdown img.flag").addClass("flagvisibility");

            $(".dropdown dt a").click(function() {
                $(".dropdown dd ul").toggle();
            });
                        
            $(".dropdown dd ul li a").click(function() {
                var text = $(this).html();
                $(".dropdown dt a span").html(text);
                $(".dropdown dd ul").hide();
                $("#result").html("Selected value is: " + getSelectedValue("sample"));
            });
                        
            function getSelectedValue(id) {
                return $("#" + id).find("dt a span.value").html();
            }

            $(document).bind('click', function(e) {
                var $clicked = $(e.target);
                if (! $clicked.parents().hasClass("dropdown"))
                    $(".dropdown dd ul").hide();
            });


            $("#flagSwitcher").click(function() {
                $(".dropdown img.flag").toggleClass("flagvisibility");
            });

			var inputQuantity = document.querySelector('input[name="quantity"]');
			var selectSize = document.querySelector('select[name="size"]');
			var inputSize = document.querySelector('input[name="productattribute_id"]');
			selectSize.onchange = function() {
				inputQuantity.max = this.querySelector("option:checked").dataset.stock;
				inputSize.value = this.value;
			}

			
        });
     </script>
     <!----details-product-slider--->
				<!-- Include the Etalage files -->
					<link rel="stylesheet" href="css/etalage.css">
					<script src="js/jquery.etalage.min.js"></script>
				<!-- Include the Etalage files -->
				<script>
						jQuery(document).ready(function($){
			
							$('#etalage').etalage({
								thumb_image_width: 300,
								thumb_image_height: 400,
								
								// show_hint: true,
								// click_callback: function(image_anchor, instance_id){
								// 	alert('Callback example:\nYou clicked on an image with the anchor: "'+image_anchor+'"\n(in Etalage instance: "'+instance_id+'")');
								// }
							});
							// This is for the dropdown list example:
							$('.dropdownlist').change(function(){
								etalage_show( $(this).find('option:selected').attr('class') );
							});

					});
				</script>
				<!----//details-product-slider--->	
</head>
<body>
	<div class="header">
		<div class="container">
			<div class="row">
			  <div class="col-md-12">
				 <div class="header-left">
					 <div class="logo">
						<a href="index.php"><img src="images/logo.png" alt=""/></a>
					 </div>
					 <div class="menu">
						  <a class="toggleMenu" href="#"><img src="images/nav.png" alt="" /></a>
						    <ul class="nav" id="nav">
						    	<li class="current"><a href="index.php">Shop</a></li>
						    	<li><a href="team.php">Team</a></li>
						    	<li><a href="index.php">Events</a></li>
						    	<li><a href="experiance.php">Experiance</a></li>
						    	<li><a href="categories.php">Company</a></li>
								<li><a href="contact.php">Contact</a></li>								
								<div class="clear"></div>
							</ul>
							<script type="text/javascript" src="js/responsive-nav.js"></script>
				    </div>							
	    		    <div class="clear"></div>
	    	    </div>
	            <div class="header_right">
	    		  <!-- start search-->
				   <div class="search-box">
							<div id="sb-search" class="sb-search">
								<form>
									<input class="sb-search-input" placeholder="Enter your search term..." type="search" name="search" id="search">
									<input class="sb-search-submit" type="submit" value="">
									<span class="sb-icon-search"> </span>
								</form>
							</div>
						</div>
						<!----search-scripts---->
						<script src="js/classie.js"></script>
						<script src="js/uisearch.js"></script>
						<script>
							new UISearch( document.getElementById( 'sb-search' ) );
						</script>
				    <ul class="icon1 sub-icon1 profile_img">
					 <li><a class="active-icon c1" href="#"> </a>
						<ul class="sub-icon1 list">
						  <div class="clear"></div>
						  <li class=><img src="images/<?php echo $item[0]["image"]; ?>" alt=""/></li>
						  <li class="list_desc"><h4><a href="#"><?php echo $item[0]["name"]?></a></h4>
						  <span class="actual"><?php echo $item[0]["price"]?></span></li>
						  <div class="login_buttons">
							 <div class="check_button"><a href="checkout.php">Check </a></div>
							 <div class="login_button"><a href="login.php">Login</a></div>
							 <div class="clear"></div>
						  </div>
						  <div class="clear"></div>
						</ul>
					 </li>
				   </ul>
		        <div class="clear"></div>
	       </div>
	      </div>
		 </div>
	    </div>
	  </div>
     <div class="main">
      <div class="shop_top">
		<div class="container">
			<div class="row">
				<div class="col-md-9 single_left">
				   <div class="single_image">
					     <ul id="etalage">
							<li>								
								<img class="etalage_thumb_image" src="images/<?php echo $item[0]["image"]; ?>" alt="" />
								<img class="etalage_source_image" src="images/<?php echo $item[0]["image"]; ?>" alt=""  />
							</li>
<?php
    if ($attributes[0]["stock"] > 0) {
?>
							<!--<li>
								<img class="etalage_thumb_image" src="images/4.jpg" />
								<img class="etalage_source_image" src="images/4.jpg" />
							</li>
							<li>
								<img class="etalage_thumb_image" src="images/5.jpg" />
								<img class="etalage_source_image" src="images/5.jpg" />
							</li>
							<li>
								<img class="etalage_thumb_image" src="images/6.jpg" />
								<img class="etalage_source_image" src="images/6.jpg" />
							</li>
							<li>
								<img class="etalage_thumb_image" src="images/7.jpg" />
								<img class="etalage_source_image" src="images/7.jpg" />
							</li>
							<li>
								<img class="etalage_thumb_image" src="images/8.jpg" />
								<img class="etalage_source_image" src="images/8.jpg" />
							</li>
							<li>
								<img class="etalage_thumb_image" src="images/9.jpg" />
								<img class="etalage_source_image" src="images/9.jpg" />
							</li>-->
						</ul>
					    </div>
				        <!-- end product_slider -->
				        <div class="single_right">
				        	<h3><?php echo $item[0]["name"]?>  </h3>
					
							<ul class="options" name="size" required>
									<h4 class="m_12">Size</h4>
									
										<label> 

											<select name="size">
<?php
	foreach($attributes as $attribute) {
?>
												<option value="<?php echo $attribute["productattribute_id"];?>" data-stock="<?php echo $attribute["stock"]; ?>"><?php echo $attribute["description"]; ?></option>
<?php
    }
?>
											</select>
										</label>
								
								
							</ul>											
				        	<div class="btn_form"></div>						
				          </div>
				        <div class="clear"> </div>
				</div>
				<div class="col-md-3">
				  <div class="box-info-product">
					<p class="price2"><?php echo $item[0]["price"]?>&euro;</p>
					 <form class="cart" method="post" action="cart.php">
					       <ul class="product-qty">
							   
									<label>Quantidade:
										<input type="number" name="quantity"  value="1" min="1" max="<?php echo $attributes[0]["stock"];?>" required>
									</label>
								
							</ul>	
					
								<input type="hidden" name="product_id" value="<?php echo $item[0]["product_id"];?>">
								<input type="hidden" name="productattribute_id" value="<?php echo $attributes[0]["productattribute_id"]; ?>">
								<button type="submit" name="submit" value="Adicionar" class="exclusive">
							   		<span>Adicionar</span>
								</button>										
					</form>		
				 </div>
			   </div>
			</div>						
<?php
    }
    else {
        echo'<p> Artigo Esgotado!<p/>';
    }
?>
			<div class="desc">
			   	<h4>Description</h4>
			   	<p class="m_10"><?php echo $item[0]["description"]?></p>
			</div>
			
	  <div class="footer">
			<div class="container">
				<div class="row">
					<div class="col-md-3">
						<ul class="footer_box">
							<h4>Products</h4>
							<li><a href="#">Mens</a></li>
							<li><a href="#">Womens</a></li>
							<li><a href="#">Youth</a></li>
						</ul>
					</div>
					<div class="col-md-3">
						<ul class="footer_box">
							<h4>About</h4>
							<li><a href="#">Careers and internships</a></li>
							<li><a href="#">Sponserships</a></li>
							<li><a href="#">team</a></li>
							<li><a href="#">Catalog Request/Download</a></li>
						</ul>
					</div>
					<div class="col-md-3">
						<ul class="footer_box">
							<h4>Customer Support</h4>
							<li><a href="#">Contact Us</a></li>
							<li><a href="#">Shipping and Order Tracking</a></li>
							<li><a href="#">Easy Returns</a></li>
							<li><a href="#">Warranty</a></li>
							<li><a href="#">Replacement Binding Parts</a></li>
						</ul>
					</div>
					<div class="col-md-3">
						<ul class="footer_box">
							<h4>Newsletter</h4>
							<div class="footer_search">
				    		   <form>
				    			<input type="text" value="Enter your email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Enter your email';}">
				    			<input type="submit" value="Go">
				    		   </form>
					        </div>
							<ul class="social">	
							  <li class="facebook"><a href="#"><span> </span></a></li>
							  <li class="twitter"><a href="#"><span> </span></a></li>
							  <li class="instagram"><a href="#"><span> </span></a></li>	
							  <li class="pinterest"><a href="#"><span> </span></a></li>	
							  <li class="youtube"><a href="#"><span> </span></a></li>										  				
						    </ul>
		   				</ul>
					</div>
				</div>
				<div class="row footer_bottom">
				    <div class="copy">
			           <p>© 2014 Template by <a href="http://w3layouts.com" target="_blank">w3layouts</a></p>
		            </div>
					  <dl id="sample" class="dropdown">
				        <dt><a href="#"><span>Change Region</span></a></dt>
				        <dd>
				            <ul>
				                <li><a href="#">Australia<img class="flag" src="images/as.png" alt="" /><span class="value">AS</span></a></li>
				                <li><a href="#">Sri Lanka<img class="flag" src="images/srl.png" alt="" /><span class="value">SL</span></a></li>
				                <li><a href="#">Newziland<img class="flag" src="images/nz.png" alt="" /><span class="value">NZ</span></a></li>
				                <li><a href="#">Pakistan<img class="flag" src="images/pk.png" alt="" /><span class="value">Pk</span></a></li>
				                <li><a href="#">United Kingdom<img class="flag" src="images/uk.png" alt="" /><span class="value">UK</span></a></li>
				                <li><a href="#">United States<img class="flag" src="images/us.png" alt="" /><span class="value">US</span></a></li>
				            </ul>
				         </dd>
	   				  </dl>
   				</div>
			</div>
		</div>

</body>	
</html>