<?php
    require_once("config.php");

    if ( !isset($_SESSION["cart"]) ) {
        $_SESSION["cart"] = array();
    }
    // unset( $_SESSION["cart"] );
 
    if ( isset($_POST["submit"]) ) {
        if( !empty($_POST["quantity"]) && 
            !empty($_POST["product_id"]) &&
            !empty($_POST["productattribute_id"]) &&
            is_numeric($_POST["quantity"]) &&
            $_POST["quantity"] > 0
        ) {            
        
            $query = $db->prepare("
                SELECT p.product_id, p.name , p.price , a.description, a.productattribute_id
                FROM products AS p
				INNER JOIN productattributes AS a USING(product_id)
                WHERE p.product_id = ? AND a.stock >= ? AND a.productattribute_id = ?
            ");

            $query -> execute( 
                array($_POST["product_id"], $_POST["quantity"], $_POST["productattribute_id"])
            );
            $result = $query->fetchAll(PDO::FETCH_ASSOC);
 


            if(!empty($result)) {
                /*  validar*/
				 $_SESSION["cart"][ $result[0]["product_id"] ] = array(
                    "product_id" =>  $result[0]["product_id"],
                    "quantity" => (int)$_POST["quantity"],
                    "name"=> $result[0]["name"],
                    "price"=> $result[0]["price"],
					"productattribute_id" => $result[0]["productattribute_id"],
                    "description"=> $result[0]["description"]
                );
            }
        }
    }    
   	//   echo "<pre>"; print_r($result); echo "</pre>";
?>
<!DOCTYPE HTML>
<html>
<head>
<title>Carrinho de Compras</title>
    <script>
        window.onload = function() {

            var buttons = document.querySelectorAll("button");
            
            for (var i = 0; i < buttons.length; i++) {
                buttons[i].onclick = function() {

                    var product_id = this.dataset.id;
                    this.parentNode.parentNode.parentNode.removeChild( this.parentNode.parentNode);

					var lines = document.querySelectorAll("table tr");					
					console.log(lines);

					if (lines.length === 2) {
						console.log("coise");
						document.getElementById("tabela").remove();
						
						
					};

                    var xhr = new XMLHttpRequest();
                    xhr.open("POST", "requests.php", true);
                    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
                    xhr.send("product_id=" +product_id+"&request=remove" );

                }
            }

        }
    </script>
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<link href="css/style.css" rel='stylesheet' type='text/css' />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<script src="js/jquery.min.js"></script>
<script type="text/javascript">
        $(document).ready(function() {
            $(".dropdown img.flag").addClass("flagvisibility");

            $(".dropdown dt a").click(function() {
                $(".dropdown dd ul").toggle();
            });
                        
            $(".dropdown dd ul li a").click(function() {
                var text = $(this).html();
                $(".dropdown dt a span").html(text);
                $(".dropdown dd ul").hide();
                $("#result").html("Selected value is: " + getSelectedValue("sample"));
            });
                        
            function getSelectedValue(id) {
                return $("#" + id).find("dt a span.value").html();
            }

            $(document).bind('click', function(e) {
                var $clicked = $(e.target);
                if (! $clicked.parents().hasClass("dropdown"))
                    $(".dropdown dd ul").hide();
            });


            $("#flagSwitcher").click(function() {
                $(".dropdown img.flag").toggleClass("flagvisibility");
            });
        });
     </script>
 </head>
<body>
	<div class="header">
		<div class="container">
			<div class="row">
			  <div class="col-md-12">
				 <div class="header-left">
					 <div class="logo">
						<a href="index.php"><img src="images/logo.png" alt=""/></a>
					 </div>
					 <div class="menu">
						  <a class="toggleMenu" href="#"><img src="images/nav.png" alt="" /></a>
						    <ul class="nav" id="nav">
						    	<li><a href="index.php">Shop</a></li>
						    	<li><a href="team.php">Team</a></li>
						    	<li><a href="index.php">Events</a></li>
						    	<li><a href="experiance.php">Experiance</a></li>
						    	<li><a href="categories.php">Company</a></li>
								<li><a href="contact.php">Contact</a></li>								
								<div class="clear"></div>
							</ul>
							<script type="text/javascript" src="js/responsive-nav.js"></script>
				    </div>							
	    		    <div class="clear"></div>
	    	    </div>
	            <div class="header_right">
	    		  <!-- start search-->
				   <div class="search-box">
							<div id="sb-search" class="sb-search">
								<form>
									<input class="sb-search-input" placeholder="Enter your search term..." type="search" name="search" id="search">
									<input class="sb-search-submit" type="submit" value="">
									<span class="sb-icon-search"> </span>
								</form>
							</div>
						</div>
						<!----search-scripts---->
						<script src="js/classie.js"></script>
						<script src="js/uisearch.js"></script>
						<script>
							new UISearch( document.getElementById( 'sb-search' ) );
						</script>
		
						  <div class="clear"></div>
						</ul>
					 </li>
				   </ul>
		        <div class="clear"></div>
	       </div>
	      </div>
		 </div>
	    </div>
	  </div>
     <div class="main">
      <div class="shop_top">
		<div class="container">
			<?php
    if (!empty($_SESSION["cart"])) {
?>
    <table border="1" id="tabela">
        <tr >
            <th>Name</th>
            <th>Quantity</th>
            <th>Price</th>
            <th>Size</th>
            <th>Total</th>
            <th>Remove</th>
        </tr>
<?php
        $total =  0;

        foreach($_SESSION["cart"] as $item) {
            $total += $item["quantity"] * $item["price"];
?>
        <tr>
            <td><?php echo $item["name"];?></td>
            <td><?php echo $item["quantity"];?></td>
            <td><?php echo $item["price"];?></td>
            <td><?php echo $item["description"];?></td>
            <td><?php echo $item["quantity"] * $item["price"];?></td>
            <td><button data-id="<?php echo $item["product_id"];?>" type="button">X</td>
        </tr>
<?php
        }
?>
        <tr>    
            <td colspan="4">Total</td>
            <td ><?php echo $total; ?></td>
            <td></td>
        </tr>
   </table>
   <nav>
        <a href="checkout.php">Finalizar Compra</a>
        <a href="./">Voltar</a>
   </nav>
<?php
    }
?>
	     </div>
	   </div>
	  </div>
	  <div class="footer">
			<div class="container">
				<div class="row">
					<div class="col-md-3">
						<ul class="footer_box">
							<h4>Products</h4>
							<li><a href="#">Mens</a></li>
							<li><a href="#">Womens</a></li>
							<li><a href="#">Youth</a></li>
						</ul>
					</div>
					<div class="col-md-3">
						<ul class="footer_box">
							<h4>About</h4>
							<li><a href="#">Careers and internships</a></li>
							<li><a href="#">Sponserships</a></li>
							<li><a href="#">team</a></li>
							<li><a href="#">Catalog Request/Download</a></li>
						</ul>
					</div>
					<div class="col-md-3">
						<ul class="footer_box">
							<h4>Customer Support</h4>
							<li><a href="#">Contact Us</a></li>
							<li><a href="#">Shipping and Order Tracking</a></li>
							<li><a href="#">Easy Returns</a></li>
							<li><a href="#">Warranty</a></li>
							<li><a href="#">Replacement Binding Parts</a></li>
						</ul>
					</div>
					<div class="col-md-3">
						<ul class="footer_box">
							<h4>Newsletter</h4>
							<div class="footer_search">
				    		   <form>
				    			<input type="text" value="Enter your email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Enter your email';}">
				    			<input type="submit" value="Go">
				    		   </form>
					        </div>
							<ul class="social">	
							  <li class="facebook"><a href="#"><span> </span></a></li>
							  <li class="twitter"><a href="#"><span> </span></a></li>
							  <li class="instagram"><a href="#"><span> </span></a></li>	
							  <li class="pinterest"><a href="#"><span> </span></a></li>	
							  <li class="youtube"><a href="#"><span> </span></a></li>										  				
						    </ul>
		   				</ul>
					</div>
				</div>
				<div class="row footer_bottom">
				    <div class="copy">
			           <p>© 2014 Template by <a href="http://w3layouts.com" target="_blank">w3layouts</a></p>
		            </div>
					  <dl id="sample" class="dropdown">
				        <dt><a href="#"><span>Change Region</span></a></dt>
				        <dd>
				            <ul>
				                <li><a href="#">Australia<img class="flag" src="images/as.png" alt="" /><span class="value">AS</span></a></li>
				                <li><a href="#">Sri Lanka<img class="flag" src="images/srl.png" alt="" /><span class="value">SL</span></a></li>
				                <li><a href="#">Newziland<img class="flag" src="images/nz.png" alt="" /><span class="value">NZ</span></a></li>
				                <li><a href="#">Pakistan<img class="flag" src="images/pk.png" alt="" /><span class="value">Pk</span></a></li>
				                <li><a href="#">United Kingdom<img class="flag" src="images/uk.png" alt="" /><span class="value">UK</span></a></li>
				                <li><a href="#">United States<img class="flag" src="images/us.png" alt="" /><span class="value">US</span></a></li>
				            </ul>
				         </dd>
	   				  </dl>
   				</div>
			</div>
		</div>
</body>	
</html>