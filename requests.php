<?php

    require_once("config.php");
    
    if(isset($_POST["request"])) {

        if($_POST["request"] === "remove" && isset($_POST["product_id"])) {
            unset( $_SESSION["cart"][ (int)$_POST["product_id"] ] );
            die(1);
        }

    }

?>