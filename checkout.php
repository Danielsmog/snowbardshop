<?php   
   require_once ("config.php");

   if (isset($_SESSION["user_id"]) ) {

       if (!empty($_SESSION["cart"]) ) {

           $query = $db->prepare("
                INSERT INTO orders
                (user_id, order_date, paid)
                VALUES(?, NOW(), 0)
           ");

           $query->execute(
               array($_SESSION["user_id"])
           );
           $order_id = $db->lastInsertId(); 

           foreach ($_SESSION["cart"] as $item) {

                $query = $db->prepare("
                    INSERT INTO orderdetails
                    (order_id, product_id, quantity, price, size)
                    VALUES(?, ?, ?, ?, ?)
                ");

                $query->execute( 
                    array(
                        $order_id,
                        $item["product_id"],
                        $item["quantity"],
                        $item["price"],
                        $item["description"]
                    )
                );

                $query = $db->prepare("
                    UPDATE products
                    SET stock = stock - ?
                    WHERE product_id = ?
                ");
                
                $query->execute(
                    array(
                        $item["quantity"],
                        $item["product_id"] 
                        )
                );
                	
           }
           unset($_SESSION["cart"]);
           die("A sua compra foi recebida, pague com a seguinte ref: MB 213213 5468974643 543 ");
       }
       else {
           header("Location: ./");
       }
       
   }
   else {
       header("Location: login.php");
   }

?>